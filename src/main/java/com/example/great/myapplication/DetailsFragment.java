package com.example.great.myapplication;

import android.Manifest;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class DetailsFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_BITMAP = "bitmap";
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST = 1;
    private Bitmap mBitmapPreview;
    private String urlImageFull;
    private ProgressBar mProgressBarLoading;
    private OnFragmentInteractionListener mListener;
    private Button mBtnWallpaper;
    private Button mBtnBaixar;
    private Button mBtnCompartilhar;
    private String  mFilePath = Environment.getExternalStorageDirectory() + "/temporary_file.jpg";
    private Target mTarget;

    public static DetailsFragment newInstance(String urlImage) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_BITMAP, urlImage);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            urlImageFull = getArguments().getString(ARG_BITMAP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        final ImageView imageView = view.findViewById(R.id.img_preview);

        mBtnWallpaper = view.findViewById(R.id.btn_uasar_como);
        mBtnBaixar = view.findViewById(R.id.btn_baixar);
        mBtnCompartilhar = view.findViewById(R.id.btn_compartilhar);
        mProgressBarLoading = view.findViewById(R.id.pb_loading);

        mBtnCompartilhar.setOnClickListener(this);
        mBtnBaixar.setOnClickListener(this);
        mBtnWallpaper.setOnClickListener(this);

        mTarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                mProgressBarLoading.setVisibility(View.GONE);
                mBitmapPreview = bitmap;
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                mProgressBarLoading.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Erro ao carregar imagem", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        Picasso.get().load(urlImageFull).into(mTarget);

        return view;
    }

    // TODO:
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_uasar_como:
                if(checkImageIsLoading()){
                    Toast.makeText(getActivity(), "Carregando imagem", Toast.LENGTH_SHORT).show();
                } else {
                    setWallpaper();
                }
                break;
            case R.id.btn_baixar:
                if(checkImageIsLoading()){
                    Toast.makeText(getActivity(), "Carregando imagem", Toast.LENGTH_SHORT).show();
                } else {
                    baixarImagem();
                }
                break;
            case  R.id.btn_compartilhar:
                if(checkImageIsLoading()){
                    Toast.makeText(getActivity(), "Carregando imagem", Toast.LENGTH_SHORT).show();
                } else {
                    compartilharImagem();
                }
                break;
        }
    }

    private boolean checkImageIsLoading() {
        return mProgressBarLoading != null && mProgressBarLoading.getVisibility() == View.VISIBLE;
    }

    private void compartilharImagem() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        if(baixarImagem()) {
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFilePath));
            startActivity(Intent.createChooser(share, "Share Image"));
        }
    }

    private boolean baixarImagem() {
        int check = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (check == PackageManager.PERMISSION_GRANTED) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            mBitmapPreview.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File file = new File(mFilePath);
            try {
                if(!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream fo = new FileOutputStream(file);
                fo.write(bytes.toByteArray());
                Toast.makeText(getActivity(), "Arquivo salvo em " + mFilePath, Toast.LENGTH_SHORT).show();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Erro ao salvar arquivo", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1024);
        }

        return false;

    }

    private void setWallpaper() {
        WallpaperManager myWallpaperManager
                = WallpaperManager.getInstance(getActivity());
        try {
            myWallpaperManager.setBitmap(mBitmapPreview);
            Toast.makeText(getActivity(), "Adicionado como Wallpaper", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(getActivity(), "Erro ao adicionar " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public static byte[] convertBitmapToArray(Bitmap bitmap){
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, blob);
        return blob.toByteArray();
    }

    public Bitmap convertArrayToBitmap(byte[] arrayDatas){
        return BitmapFactory.decodeByteArray(arrayDatas, 0, arrayDatas.length);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST:
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    baixarImagem();
                } else {
                    Toast.makeText(getActivity(), "Você precisa fornecer esta permissão para baixar o arquivo",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public Boolean areThereAnyPermissionGranted(int[] grantResults) {
        return grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

}
