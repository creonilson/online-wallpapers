package com.example.great.myapplication.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.great.myapplication.ConnectionListener;
import com.example.great.myapplication.DetailsFragment;
import com.example.great.myapplication.R;
import com.example.great.myapplication.adapter.MyAdapter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class MainActivity extends AppCompatActivity implements ConnectionListener, View.OnClickListener,
        TextView.OnEditorActionListener, MyAdapter.ImageEventListener {

    private final static String GOOGLE_URL = "https://www.google.com/search?q=";
    private final static String SEARCH_IMAGE = "&tbm=isch&tbs=isz:lt,islt:2mp";//&tbs=isz:lt,islt:svga
    private final static String SEARCH_IMAGE_HD = "&tbs=iszw:400,iszh:300,isz:lt,islt:qsvga&tbm=isch";
    private final static String NEXT_PAGE = "&start=";
    public static final String TAG_IMG = "img";
    public static final String ATTR_SRC = "src";
    public static final int DEFAULT_NUMBER_OF_ITEMS = 20;
    private Map<String, String> mImagesUrlMap;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private LinearLayout mLnFooter;
    private int mCurrentPage = 10;
    private ImageView mBtnMenos;
    private FloatingActionButton mFab;
    private SearchView mEtSeach;
    private Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolBar = findViewById(R.id.toolbar);

        setSupportActionBar(mToolBar);

        mEtSeach = findViewById(R.id.et_seach);
        mLnFooter = findViewById(R.id.ln_footer);
        ImageView btnPlus = findViewById(R.id.btn_mais);
        mBtnMenos = findViewById(R.id.btn_menos);
        mFab = findViewById(R.id.fab);
        mRecyclerView = findViewById(R.id.rv_images);
        btnPlus.setOnClickListener(this);
        mBtnMenos.setOnClickListener(this);
        mFab.setOnClickListener(this);
        //mEtSeach.setOnEditorActionListener(this);
        this.mEtSeach.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                MainActivity.this.loadContent();
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        this.mEtSeach.setIconifiedByDefault(false);
        this.mEtSeach.setFocusable(false);
        mImagesUrlMap = new HashMap<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy < 0){
                    mLnFooter.setVisibility(View.GONE);
                    mFab.setVisibility(View.VISIBLE);
                }else{
                    mLnFooter.setVisibility(View.VISIBLE);
                    mFab.setVisibility(View.GONE);
                }
            }
        });
    }


    private void loadContent() {
        UrlConnectAsync urlConnectAsync = new UrlConnectAsync(this.mEtSeach.getQuery().toString(), this);
        urlConnectAsync.execute(mCurrentPage);
        urlConnectAsync.setListener(this);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
        if(fragment != null){
            getFragmentManager().beginTransaction().
                    remove(fragment).commit();
        }
    }

    @Override
    public void onConnectionFinished() {
        MyAdapter adapter = new MyAdapter(new ArrayList<>(mImagesUrlMap.keySet()), this);
        mRecyclerView.setAdapter(adapter);
        adapter.setmScrollListener(this);
        if(mCurrentPage >= DEFAULT_NUMBER_OF_ITEMS){
            mBtnMenos.setEnabled(true);
        }else{
            mBtnMenos.setEnabled(false);
        }
    }

    @Override
    public void onSrollFinished() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        mLnFooter.startAnimation(animation);
        mLnFooter.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClickListener(Bitmap image, String urlKey) {
        showdetailsFragment(image,urlKey);
        mFab.setVisibility(View.GONE);
    }

    public void showdetailsFragment(Bitmap bitmap, String urlKey){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out);
        DetailsFragment newFragment = DetailsFragment.newInstance(mImagesUrlMap.get(urlKey));
        ft.replace(R.id.container, newFragment, "detailFragment");
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_mais:
                mCurrentPage += 10;
                loadContent();
                break;
            case R.id.btn_menos:
                mCurrentPage -= 10;
                loadContent();
                break;
            case R.id.fab:
                if (!mImagesUrlMap.isEmpty()) {
                    mImagesUrlMap.clear();
                }
                loadContent();
                break;
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            loadContent();
            return true;
        }
        return false;
    }

    public class UrlConnectAsync extends AsyncTask<Integer, Void, Void>{

        private String mKey;
        private ConnectionListener listener;
        private Context mContext;

        public void setListener(ConnectionListener listener) {
            this.listener = listener;
        }

        public UrlConnectAsync(String key, Context context) {
            this.mKey = key;
            this.mContext = context;
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setTitle(getString(R.string.st_aguarde));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            Document document = null;
            try {
                document = Jsoup.connect(GOOGLE_URL + mKey + SEARCH_IMAGE_HD + NEXT_PAGE + params[0])
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30").cookie("auth", "token").timeout(6000).get();
                mImagesUrlMap = getAllImagesFromVector(document);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listener.onConnectionFinished();
            mProgressDialog.dismiss();
        }
    }

    private List<String> getAllImagesFrom(Document document){ //rg_ic rg_i
        List<String> urlList = new ArrayList<>();
        Elements elements = document.getElementsByTag(TAG_IMG);
        for (Element el : elements) {
            String urlImage = el.attr("data-src");
            //String urlFull = el.parent().getElementById("url").html();
            //Log.i("debug", "imege: " + urlImage + " urlfull: " + urlFull);
            if(urlImage.contains("textinputassistant")) continue;
            urlList.add(urlImage);
        }
        return urlList;
    }

    private Map<String, String> getAllImagesFromVector(Document document) {
        Vector<String> urlList = new Vector<>();
        Map<String, String> mapaUrl = new HashMap<>();
        for (Element o : document.getElementsByAttribute("jscontroller")) {
            Element divTumbnail = o.child(0).child(1);
            String urlImageTumbnail = divTumbnail.attr("data-src");
            if(urlImageTumbnail.isEmpty()) continue;

            Element divUrlHD = o.child(1);
            String fullUrl = divUrlHD.html();
            //String value = "{first_name = naresh,last_name = kumar,gender = male}";

            if (fullUrl.length() > 0) {
                fullUrl = fullUrl.substring(1, fullUrl.length()-1);           //remove curly brackets
                String[] keyValuePairs = fullUrl.split(",");
                for (String keyValuePair : keyValuePairs) {
                    if(keyValuePair.contains("http") && keyValuePair.contains(".jpg")) {
                        fullUrl = keyValuePair.substring(6).replace("\"", "");
                        mapaUrl.put(urlImageTumbnail, fullUrl);
                    }
                }
            }
        }
        return mapaUrl;
    }

}
