package com.example.great.myapplication.asyncs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.great.myapplication.OnImageLoadListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Creonilso on 07/01/2016.
 */
public class AddImagesAsync extends AsyncTask<Void, Void, Void> {

    private List<String> mListUrls;
    private OnImageLoadListener mImageLoadListener;

    public AddImagesAsync(List<String> urls, OnImageLoadListener onImageLoadListener) {
        this.mListUrls = urls;
        this.mImageLoadListener = onImageLoadListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {

        for (String url : mListUrls) {
            Bitmap bitmap = getBitmapFromURL(url);
            //passa cada imagem carregada para o adapter
            mImageLoadListener.onImageLoaded(bitmap);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

    }

    /**
     * @param stringUrl URL para fazer download da imagem
     * @return imagem como bitmap
     */
    public static Bitmap getBitmapFromURL(String stringUrl) {
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            Log.i(AddImagesAsync.class.getName(),"falha no download " + e.getMessage());
            return null;
        }
    }

}
