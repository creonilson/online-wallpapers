package com.example.great.myapplication;

import android.graphics.Bitmap;

/**
 * Created by Creonilso on 07/01/2016.
 */
public interface OnImageLoadListener {
    void onImageLoaded(Bitmap mBitmap);
}
