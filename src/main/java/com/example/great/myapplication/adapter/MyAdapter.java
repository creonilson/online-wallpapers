package com.example.great.myapplication.adapter;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.great.myapplication.OnImageLoadListener;
import com.example.great.myapplication.R;
import com.example.great.myapplication.asyncs.AddImagesAsync;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by great on 08/01/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> implements OnImageLoadListener {
    private List<Bitmap> mImages;
    private List<String> mUrlsImage;
    private ImageEventListener mImageEventListener;

    public void setmScrollListener(ImageEventListener imageEventListener) {
        this.mImageEventListener = imageEventListener;
    }

    @Override
    public void onImageLoaded(Bitmap mBitmap) {
        mImages.add(mBitmap);

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView mImageView;
        public ProgressBar mPbDialog;
        public ViewHolder(View v) {
            super(v);
            mImageView = v.findViewById(R.id.iv_wallpaper);
            mPbDialog = v.findViewById(R.id.pb_loading);
            mImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.iv_wallpaper){
                mImageEventListener.onItemClickListener(mImages.get(getAdapterPosition()), mUrlsImage.get(getAdapterPosition()) );
            }
        }
    }

    public MyAdapter(List<String> urlsImages, ImageEventListener mImageEventListener) {
        this.mImageEventListener = mImageEventListener;
        mImages = new ArrayList<>();
        mUrlsImage = urlsImages;
        AddImagesAsync addImagesAsync = new AddImagesAsync(mUrlsImage, this);
        addImagesAsync.execute();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(mImages.size()-1 >= position) {
            holder.mImageView.setImageBitmap(mImages.get(position));
            holder.mPbDialog.setVisibility(View.GONE);
        }

        if(position == mUrlsImage.size()-1){
            //TODO fazer algo quando chegar ao final da lista
           // mScrollListener.onSrollFinished();
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mUrlsImage.size();
    }

    public interface ImageEventListener{
        void onSrollFinished();
        void onItemClickListener(Bitmap image, String urlKey);
    }
}
