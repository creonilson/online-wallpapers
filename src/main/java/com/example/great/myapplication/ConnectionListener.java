package com.example.great.myapplication;

/**
 * Created by great on 08/01/2016.
 */
public interface ConnectionListener {
    void onConnectionFinished();
}
